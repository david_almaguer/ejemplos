#include <windows.h>
#include <commctrl.h>
#include <dinput.h>
#include <dinputd.h>

struct DI_ENUM_CONTEXT
{
    DIJOYCONFIG* pPreferredJoyCfg;
    bool bPreferredJoyCfgValid;
};

#define SAFE_DELETE(p)  { if(p) { delete (p);     (p)=NULL; } }
#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }

LPDIRECTINPUT8          g_pDI = NULL;
LPDIRECTINPUTDEVICE8    g_pJoystick = NULL;

DIJOYSTATE2 gamepad_state;

BOOL CALLBACK    EnumObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext );
BOOL CALLBACK    EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance, VOID* pContext );
HRESULT InitDirectInput( HWND hDlg );
VOID FreeDirectInput();
HRESULT UpdateInputState( HWND hDlg );

class GamepadGeneric
{
public:
	bool Boton[128];
	float leftX,leftY,rightX,rightY;
	int pov1,pov2,pov3,pov4;

	GamepadGeneric()
	{
		InitDirectInput(NULL);
	};

	void Update()
	{
		UpdateInputState(NULL);

		// obtener el estado de los botones
		for(int i=0; i < 128; i++)
			Boton[i] = gamepad_state.rgbButtons[i];

		//obtener el estado de los joysticks y reducir el valor a un numero mas fiable
		leftX = (gamepad_state.lX - 32767.0f) / 256.0f;
		leftY = (gamepad_state.lY - 32767.0f) / 256.0f;

		rightX = (gamepad_state.lZ - 32767.0f) / 256.0f;
		rightY  = (gamepad_state.lRz - 32767.0f) / 256.0f;

		//obtener el estado del d-pad
		pov1 = gamepad_state.rgdwPOV[0];
	};

	~GamepadGeneric()
	{
		FreeDirectInput();
	};
};

BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance,
											 VOID* pContext )
		{
			DI_ENUM_CONTEXT* pEnumContext = ( DI_ENUM_CONTEXT* )pContext;
			HRESULT hr;

			if( pEnumContext->bPreferredJoyCfgValid &&
				!IsEqualGUID( pdidInstance->guidInstance, pEnumContext->pPreferredJoyCfg->guidInstance ) )
				return DIENUM_CONTINUE;
			
			hr = g_pDI->CreateDevice( pdidInstance->guidInstance, &g_pJoystick, NULL );

			if( FAILED( hr ) )
				return DIENUM_CONTINUE;

			return DIENUM_STOP;
		};

BOOL CALLBACK EnumObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi,
										   VOID* pContext )
		{
			HWND hDlg = ( HWND )pContext;

			static int nSliderCount = 0;  
			static int nPOVCount = 0;    

			if( pdidoi->dwType & DIDFT_AXIS )
			{
				DIPROPRANGE diprg;
				diprg.diph.dwSize = sizeof( DIPROPRANGE );
				diprg.diph.dwHeaderSize = sizeof( DIPROPHEADER );
				diprg.diph.dwHow = DIPH_BYID;
				diprg.diph.dwObj = pdidoi->dwType; 
				diprg.lMin = -1000;
				diprg.lMax = +1000;

				if( FAILED( g_pJoystick->SetProperty( DIPROP_RANGE, &diprg.diph ) ) )
					return DIENUM_STOP;

			}

			return DIENUM_CONTINUE;	
		};

HRESULT InitDirectInput( HWND hDlg )
		{
			HRESULT hr;

			if( FAILED( hr = DirectInput8Create( GetModuleHandle( NULL ), DIRECTINPUT_VERSION,
												 IID_IDirectInput8, ( VOID** )&g_pDI, NULL ) ) )
				return hr;


			DIJOYCONFIG PreferredJoyCfg = {0};
			DI_ENUM_CONTEXT enumContext;
			enumContext.pPreferredJoyCfg = &PreferredJoyCfg;
			enumContext.bPreferredJoyCfgValid = false;

			IDirectInputJoyConfig8* pJoyConfig = NULL;
			if( FAILED( hr = g_pDI->QueryInterface( IID_IDirectInputJoyConfig8, ( void** )&pJoyConfig ) ) )
				return hr;

			PreferredJoyCfg.dwSize = sizeof( PreferredJoyCfg );
			if( SUCCEEDED( pJoyConfig->GetConfig( 0, &PreferredJoyCfg, DIJC_GUIDINSTANCE ) ) ) // This function is expected to fail if no joystick is attached
				enumContext.bPreferredJoyCfgValid = true;
			SAFE_RELEASE( pJoyConfig );

			if( FAILED( hr = g_pDI->EnumDevices( DI8DEVCLASS_GAMECTRL,
												 EnumJoysticksCallback,
												 &enumContext, DIEDFL_ATTACHEDONLY ) ) )
				return hr;

			if( NULL == g_pJoystick )
			{
				
				return S_OK;
			}

			if( FAILED( hr = g_pJoystick->SetDataFormat( &c_dfDIJoystick2 ) ) )
				return hr;

			if( FAILED( hr = g_pJoystick->SetCooperativeLevel( hDlg, DISCL_EXCLUSIVE |
															   DISCL_FOREGROUND ) ) )
				return hr;
		
			if( FAILED( hr = g_pJoystick->EnumObjects( EnumObjectsCallback,
													   ( VOID* )hDlg, DIDFT_ALL ) ) )
				return hr;

			return S_OK;
		};

HRESULT UpdateInputState( HWND hDlg )
{
    HRESULT hr;
    TCHAR strText[512] = {0}; 
    DIJOYSTATE2 new_state;            

    if( NULL == g_pJoystick )
        return S_OK;

    hr = g_pJoystick->Poll();
    if( FAILED( hr ) )
    {
        hr = g_pJoystick->Acquire();
        while( hr == DIERR_INPUTLOST )
            hr = g_pJoystick->Acquire();

        return S_OK;
    }

    if( FAILED( hr = g_pJoystick->GetDeviceState( sizeof( DIJOYSTATE2 ), &new_state ) ) )
        return hr;

	gamepad_state = new_state;

    return S_OK;
};

VOID FreeDirectInput()
{
   if( g_pJoystick )
        g_pJoystick->Unacquire();

   SAFE_RELEASE( g_pJoystick );
    SAFE_RELEASE( g_pDI );
};