
enum GameInput
{
	Gamepad,
	Keyboard,
	Kinect
};

enum GameSound
{
	Stereo,
	Mono,
	Surround3D
};

int main()
{
	GameInput myGameInput = GameInput::Keyboard;
	GameSound myGameSound = GameSound::Stereo;

	if(myGameSound == GameSound::Stereo)
		LoadStereoSounds();
	else if(myGameSound == GameSound::Surround3D)
		Load3DSounds();

	return 0;
}

void Load3DSounds()
{
	//Carga de sonidos 3D
}
void LoadStereoSounds()
{
	//Carga de sonidos stereo
}