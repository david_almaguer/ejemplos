#include <iostream>
#include "Circulo.h"

int main()
{
	// Se declara el objeto miCirculo del tipo Circulo
	// y se manda llamar al constructor, se puede notar
	// que el Punto se inicializa dentro del parametro del constructor
	// sin necesidad de crear un objeto del tipo Punto
	Circulo miCirculo(5.6f, Circulo::Punto(5,5));

	// Otra manera es crear un objeto de tipo punto
	// y pasarlo como parametro
	Circulo::Punto puntoCentral(20, 20);
	miCirculo = Circulo::Circulo(5.6f, puntoCentral);

	float circunferencia = miCirculo.Circunferencia();
	float area = miCirculo.Area();

	printf("Area: %f \nCircunferencia: %f", area, circunferencia);

	return 0;
}