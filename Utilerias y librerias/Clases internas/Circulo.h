//En el archivo .h solo va
//la definicion de la clase

class Circulo
{
public:
	class Punto
	{
	public:
		float x;
		float y;

		Punto(float _x, float _y)
		{
			Punto::x = _x;
			Punto::y = _y;
		}
	};

	Circulo(float _radio, Punto _centro);

	float Circunferencia();
	float Area();

	float radio;
	Punto centro;
};