// En el archivo .cpp 
// va la implementación de la clase

#include "Circulo.h"
#define Pi 3.141516

// Cuando se pasa un objeto que su clase esta dentro de otra clase
// el constructor debe seguir de :_variable_del_objeto(_parametros),
// para asi inicializar el objeto dentro de la clase
Circulo::Circulo(float _radio, Punto _centro):centro(_centro.x, _centro.y)
{
	Circulo::radio = _radio;
	Circulo::centro = _centro;
}

float Circulo::Circunferencia()
{
	return Pi * (radio+radio);
}

float Circulo::Area()
{
	return Pi * (radio*radio);
}