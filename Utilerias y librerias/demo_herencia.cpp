

//Clase A que es la clase padre
class ClaseA
{
	//Todos sus atributos y metodos son publicos
public:
	int a;
	int b;

	// Constructor que inicializa las variables internas de la clase
	ClaseA()
	{
		a = 0;
		b = 0;
	}

	// Metodo que acepta dos parametros y establece valores en
	// las variables 'a' y 'b'
	void set(int param1, int param2)
	{
		a = param1;
		b = param2;
	}

	// Metodo que suma 'a' y 'b' y regresa su valor
	int suma()
	{
		return a + b;
	}
};

// Clase B que hereda de la Clase A; la etiqueta public ":public ClaseA" (vease en la definicion de la Clase B)
// permite que sus atributos puedan ser accesibles a nivel objeto
//
// ClaseB miclase; 
// miclase.a; //<=== Ejemplo de atributo accesible que le pertence a la Clase A
//
// a eso me refiero a nivel objeto, cuando ya esta declarado en memoria
class ClaseB : public ClaseA
{
public:
	//Nuevas variables definidas en la Clase B
	//que solo son accesibles en este ambito, al menos
	//que se herede la Clase B a otra clase.
	int c;
	int d;

	// Constructor de Clase B
	// Como sabemos los constructores no se heredan, 
	// asi que se define el constructor de la Clase B
	// y a su vez se manda llamar manualmente el constructor
	// de la clase A, solo si se necesita
	// esto permite inicializar 'a' y 'b' de la Clase A 
	// sin necesidad de volverlas a inicializar en la Clase B
	ClaseB() : ClaseA()
	{
		c = 0;
		d = 0;
	}

	// Metodo redifinido de la Clase A,
	// el metodo que esta en la Clase A nunca mas
	// se vuelve a llamar atraves de esta clase,
	// debido a la redefinicion de la funcion int suma()
	int suma()
	{
		// Ahora que tengo la redefinicion de suma en Clase B,
		// puedo mandar llamar a suma de la Clase A "ClaseA::suma()"
		// manualmente, solo si es necesario.
		return ClaseA::suma() + c + d;
	}
};

int main()
{
	
	ClaseA cA;
	ClaseB cB;

	cA.a = 5;
	cA.b = 10;
	int sumaCA = cA.suma();

	cB.set(10,10);
	cB.c = 10;
	cB.d = 5;
	int sumaCB = cB.suma();

	return 0;
}