#ifndef _SKYBOX_H_
#define _SKYBOX_H_

#ifdef _WINDOWS
#include <windows.h>
#endif

#include <gl/gl.h>
#include <gl/glu.h>

class Skybox
{
public:
	GLuint textura[1];
	float tamanio;
	
	Skybox(char* rutaTextura, float _tamanio);
	void Draw();
	void Update();
};

#endif