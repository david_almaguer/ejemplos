#include "mov.h"
#include <math.h>

/* Constructor */
mov::mov()
{
}

/* Destructor */
mov::~mov()
{
}

 /* 
  *Metodo que obtiene un objeto por referencia, 
  * para asi manipularlo directamente y modificarlo
  *sin tener que crear una copia del objeto y tener que retonarlo
 */
void mov::moverCubo(Cube &_cubo)
{
	static int limit = 5;
	static float ang = 0;
	ang += 1.5f;

	if(ang >= 360.0f)
		ang = 0.0f;
	_cubo.posicion.y = sin(ang *3.141516f / 180.0f) * 1;
}

void mov::moverEsfera(Dome &_dome, int vueltas)
{
	// Factores que permiten modificar la distancia de la traslacion y
	// la velocidad a la que se traslada
	float dist = 0.9f;
	float vel = 1.5f;

	// static permite que la variable solo se declara una sola vez, y mantener su valor en memoria
	static float ang = 0;
	static float tmpVueltas = 0;
	ang += vel;

	if(ang >= 360){
		ang = 0;
		tmpVueltas ++;
	}
	// angulo * PI / 180.0f => Conversion de grados a radianes

	if(tmpVueltas < vueltas)
	{
		_dome.posicion.x = cos(3.141516f * ang / 180.0f) * dist;
		_dome.posicion.y = sin(3.141516f * ang / 180.0f) * dist;
	}
}
