#ifndef _MOV_H_
#define _MOV_H_
#include "Cube.h"
#include "Dome.h"
#include <Windows.h>


class mov{
public:
	mov();
	~mov();
	
	// El paso por referencia, permite meodificar la variable directamente, en caso 
	// contrario, se crearia una copia de la variable y solo se modificaria
	// en la funcion.
	void mover(float &_x,float &_y,float &_z);
	void moverCubo(Cube &_cubo);

	void moverEsfera(Dome &_dome, int vueltas = 100);
};

#endif