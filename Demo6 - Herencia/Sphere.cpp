#include <Windows.h>
#include <math.h>
#include "TextureLoader.h"
#include "Sphere.h"
#include <math.h>

#define PI 3.141516

Sphere::Sphere(char* pathTexture, float _radius, int _stacks, int _slices)
{
	Sphere::radius = _radius;
	Sphere::slices = _slices;
	Sphere::stacks = _stacks;
	//Aqui se carga la textura por medio de la funcion LoadTexture
	//Sphere::texture = LoadTexture(pathTexture);
	LoadVertex();
	position.x = 0;
	position.y = 0;
	position.z = 0;
}

Sphere::~Sphere()
{
	glDeleteTextures(1, &texture);
}

void Sphere::LoadVertex()
{
	vertexArray = new CustomVertex[stacks * slices];
	
	for(int x = 0; x < stacks; x++)
	{
		for(int y = 0; y < slices; y++)
		{
			int arrayIndex = x * stacks + y;
			vertexArray[arrayIndex].x = (float)(radius * cos(((float)x / (float)(stacks - 1)) * PI * 2.0f) * cos(2.0f * PI * (float)y / (float)(slices - 1)));
			vertexArray[arrayIndex].y = (float)(radius * sin(((float)x / (float)(stacks - 1)) * PI * 2.0f));
			vertexArray[arrayIndex].z = (float)(radius * cos(((float)x / (float)(stacks - 1)) * PI * 2.0f) * sin(2.0f * PI * (float)y / (float)(slices - 1)));
			float len = sqrt(vertexArray[arrayIndex].x * vertexArray[arrayIndex].x + vertexArray[arrayIndex].y * vertexArray[arrayIndex].y + vertexArray[arrayIndex].z * vertexArray[arrayIndex].z);
			float u = acos(vertexArray[arrayIndex].y / len) / PI;
			float v = (atan2(vertexArray[arrayIndex].z, vertexArray[arrayIndex].x) / PI + 0.5f);
			vertexArray[arrayIndex].u = u;// 1.0f*(1.0f - (float)x / ((float)stacks - 1.0f));//u;//(float)(((((float)stacks - 1.0f - x) * 0.5f) / (float)stacks) * cos((y * 360.0f / (float)slices) * PI / 180.0f)) + 0.5f;
			vertexArray[arrayIndex].v = v;//1.0f*(1.0f - (float)y / ((float)slices - 1.0f));//v;//(float)(((((float)stacks - 1.0f - x) * 0.5f) / (float)stacks) * sin((y * 360.0f / (float)slices) * PI / 180.0f)) + 0.5f;
		}
	}
}

void Sphere::Draw()
{
	//Cambiar();
	glPushMatrix();
	glTranslatef(position.x, position.y, position.z - 4.0f);
	glRotatef(90, 1, 1, 0);
	glPushAttrib(GL_TEXTURE_BIT);
		glBindTexture(GL_TEXTURE_2D, texture);
			glBegin(GL_TRIANGLE_STRIP);

			for ( UINT y = 0; y < stacks - 1; y++ )
			{
				for ( UINT x = 0; x < slices - 1; x++ )
				{
					int lLeft =  y * slices + x;
					int lRight = (y + 1) * slices + x;
					int tLeft = y * slices + (x + 1);
					int tRight = (y + 1) * slices + (x + 1);

					glTexCoord2f(vertexArray[lLeft].u, vertexArray[lLeft].v); glVertex3f(vertexArray[lLeft].x, vertexArray[lLeft].y, vertexArray[lLeft].z);
					glTexCoord2f(vertexArray[lRight].u, vertexArray[lRight].v); glVertex3f(vertexArray[lRight].x, vertexArray[lRight].y, vertexArray[lRight].z);
					glTexCoord2f(vertexArray[tLeft].u, vertexArray[tLeft].v); glVertex3f(vertexArray[tLeft].x, vertexArray[tLeft].y, vertexArray[tLeft].z);

					glTexCoord2f(vertexArray[lRight].u, vertexArray[lRight].v); glVertex3f(vertexArray[lRight].x, vertexArray[lRight].y, vertexArray[lRight].z);
					glTexCoord2f(vertexArray[tRight].u, vertexArray[tRight].v); glVertex3f(vertexArray[tRight].x, vertexArray[tRight].y, vertexArray[tRight].z);
					glTexCoord2f(vertexArray[tLeft].u, vertexArray[tLeft].v); glVertex3f(vertexArray[tLeft].x, vertexArray[tLeft].y, vertexArray[tLeft].z);
				}
			}

			glEnd();
		glPopAttrib();
	glPopMatrix();
	Update();
}

void Sphere::Update()
{
}