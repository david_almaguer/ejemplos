#ifndef _CUBE_H_
#define _CUBE_H_

#include "Primitive.h"

class Cube : public Primitive
{
private:
	float side;
public:
	Cube();
	Cube(char* pathTexture, float _side);
	~Cube();

	void Draw();
	void Update();
};

#endif