#ifndef _SPHERE_H_
#define _SPHERE_H_

#include "Primitive.h"

//Cunado una clase hereda de otra clase es importante
//usar la etiqueta public si se desea utilizar metodos y atributos publicos 
//que estan en la clase padre, pero no en la clase derivada.
class Sphere : public Primitive
{
private:
	int slices;
	int stacks;
	float radius;
	CustomVertex *vertexArray;
	void LoadVertex();
public:
	Sphere(char* pathTexture, float _radius, int stacks, int slices);
	~Sphere();
	
	void Update();
	void Draw();

};

#endif