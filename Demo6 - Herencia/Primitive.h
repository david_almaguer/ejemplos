/*
* Clase padre que contiene, los atributos y métodos 
* que mas tienen en común las clases que se van a derivar de ella.
*/
#ifndef _PRIMITIVE_H_
#define _PRIMITIVE_H_

#include <gl/gl.h>
#include <gl/glu.h>
#include <windows.h>
#include "GraphicsHelper.h"

class Primitive
{
//Protected me permite esconder los atributos o metodos de las clases que no heredan esta misma clase
protected:
	GLuint texture;
public:
	Vector3 position;
	Vector3 rotation;
	Vector3 scale;

	void Draw()
	{
		position.x = 0;
	}

	void Update()
	{
	}

	void Cambiar()
	{
		Draw();
	}

	//Cuando se hereda de una clase padre, el constructor y destructor no se hereda
	//pero aun así cuando se manda llamar el constructor de la clase derivada 
	//automaticamente es llamado el constructor de la clase Padre.
	Primitive()
	{
		position.x = 0;
		position.y = 0;
		position.z = 0;

		rotation.x = 0;
		rotation.y = 0;
		rotation.z = 0;

		scale.x = 0;
		scale.y = 0;
		scale.z = 0;
	}

	// Virtual permite que el destructor se mande llamar en automatico, 
	//por si acaso manejara memoria dinamica este la libere.
	virtual ~Primitive()
	{
	}
};

#endif