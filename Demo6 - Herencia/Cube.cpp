#include <iostream>
#include <gl\GLAUX.H>

#include "TextureLoader.h"
#include "Cube.h"

Cube::Cube(char* pathTexture, float _side)
{
	this->position.x = 0.0f;
	this->position.y = 0.0f;
	this->position.z = 0.0f;
	
	this->side = _side;
	//Aqui se carga la textura con una funcion
	//this->texture = LoadTexture(pathTexture);
}

Cube::~Cube()
{
	glDeleteTextures(1, &texture);
}

void Cube::Draw()
{
	glPushMatrix();
	glTranslatef(1.0f, this->position.y, -4.0f);
	glRotatef(45, 0, 1, 0);

	glPushAttrib(GL_TEXTURE_BIT);
		glBindTexture(GL_TEXTURE_2D, texture);
		glBegin(GL_QUADS);
			// Front Face
			 glTexCoord2f(0.0f, 0.0f); glVertex3f(-side, -side,  side);
			 glTexCoord2f(1.0f, 0.0f); glVertex3f( side, -side,  side);
			 glTexCoord2f(1.0f, 1.0f); glVertex3f( side,  side,  side);
			 glTexCoord2f(0.0f, 1.0f); glVertex3f(-side,  side,  side);
			// Back Face
			 glTexCoord2f(1.0f, 0.0f); glVertex3f(-side, -side, -side);
			 glTexCoord2f(1.0f, 1.0f); glVertex3f(-side,  side, -side);
			 glTexCoord2f(0.0f, 1.0f); glVertex3f( side,  side, -side);
			 glTexCoord2f(0.0f, 0.0f); glVertex3f( side, -side, -side);
			// Top Face
			 glTexCoord2f(0.0f, 1.0f); glVertex3f(-side,  side, -side);
			 glTexCoord2f(0.0f, 0.0f); glVertex3f(-side,  side,  side);
			 glTexCoord2f(1.0f, 0.0f); glVertex3f( side,  side,  side);
			 glTexCoord2f(1.0f, side); glVertex3f( side,  side, -side);
			// Bottom Face
			 glTexCoord2f(1.0f, 1.0f); glVertex3f(-side, -side, -side);
			 glTexCoord2f(0.0f, 1.0f); glVertex3f( side, -side, -side);
			 glTexCoord2f(0.0f, 0.0f); glVertex3f( side, -side,  side);
			 glTexCoord2f(1.0f, 0.0f); glVertex3f(-side, -side,  side);
			// Right face
			 glTexCoord2f(1.0f, 0.0f); glVertex3f( side, -side, -side);
			 glTexCoord2f(1.0f, 1.0f); glVertex3f( side,  side, -side);
			 glTexCoord2f(0.0f, 1.0f); glVertex3f( side,  side,  side);
			 glTexCoord2f(0.0f, 0.0f); glVertex3f( side, -side,  side);
			// Left Face
			 glTexCoord2f(0.0f, 0.0f); glVertex3f(-side, -side, -side);
			 glTexCoord2f(1.0f, 0.0f); glVertex3f(-side, -side,  side);
			 glTexCoord2f(1.0f, 1.0f); glVertex3f(-side,  side,  side);
			 glTexCoord2f(0.0f, 1.0f); glVertex3f(-side,  side, -side);
		glEnd();
		glPopAttrib();
	glPopMatrix();
}