#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include <windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>

class Terrain
{
private:
	GLuint texture;
	unsigned char *height_map;
	int size;

	int sizeX;
	int sizeY;	
public:
    Terrain(int size, char* height_map_texture_path, char* texture_path);
	~Terrain();

	void LoadVertex(char*);
	void Draw();
	void Update();
};

#endif