#include "Terrain.h"
#include "TextureLoader.h"

Terrain::Terrain(int size, char* height_map_texture_path, char* texture_path)
{
	this->size = size;
	this->texture = LoadTexture(texture_path);
	LoadVertex(height_map_texture_path);
}

Terrain::~Terrain()
{
	delete this->height_map;
	glDeleteTextures(1, &this->texture);
}

void Terrain::LoadVertex(char* height_map_texture_path)
{
	AUX_RGBImageRec *TextureImage[1];
	memset(TextureImage, 0, sizeof(void *)*1);
	TextureImage[0] = auxDIBImageLoad(height_map_texture_path);
	
	sizeX= TextureImage[0]->sizeX;
	sizeY= TextureImage[0]->sizeY;

	this->height_map = new GLubyte[sizeX * sizeY];

	for(unsigned int i = 0; i < sizeY; i++)
	{
		for(unsigned int j = 0; j < sizeX; j++)
		{
			height_map[(i * sizeX + j)] = TextureImage[0]->data[(i * sizeX + j) * 3];
		}
	}
}

void Terrain::Draw()
{
	float scaleFactor = 5.0f;
	float maxHeight = 30.0;

	glPushMatrix();
	glScalef(0.2f, 0.2f, 0.2f);
	glPushAttrib(GL_TEXTURE_BIT);
		//glBindTexture(GL_TEXTURE_2D, texture);
		int avg_size = (sizeX + sizeY) / 2;

		for(int z = 0; z < sizeY - 1; ++z)
		{
			glBegin(GL_TRIANGLE_STRIP);
			for (int x = 0; x < sizeX; ++x)
			{
				float scaledHeight = height_map[z * avg_size + x] / scaleFactor;
				float nextScaledHeight = height_map[(z + 1) * avg_size + x] / scaleFactor;

				float color = 0.5f + 0.5f * scaledHeight / maxHeight;
				float nextColor = 0.5f + 0.5f * nextScaledHeight / maxHeight;
			  
				glColor4f(color, color, color, 1.0f);
				//glTexCoord2f(((float)x / (float)size), ((float)z / (float)size));//TEXTURE COMPLETE
				//glTexCoord2f(((float)x / (float)size * 5.0f), ((float)z / (float)size * 5.0f));//TEXTURE REPEAT
				glVertex3f((float)(x - size / 2.0f), scaledHeight, (float)(z - size / 2.0f));
			  
				glColor4f(nextColor, nextColor, nextColor, 1.0f);
				float numero = (float)((float)(x + 1) / (float)size);
				//glTexCoord2f(((float)(x + 1) / (float)size), ((float)(z + 1) / (float)size));//TEXTURE COMPLETE
				//glTexCoord2f(((float)x / (float)size * 5.0f), ((float)(z + 1) / (float)size * 5.0f)); //REPEAT TEXTURE
				glVertex3f((float)(x - size / 2.0f), nextScaledHeight, (float)((z + 1) - size / 2.0f));
			}
			glEnd();
		}
	glPopAttrib();
	glPopMatrix();
}