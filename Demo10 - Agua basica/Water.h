#pragma once

#include "primitive.h"
class Water : public Primitive
{
public:
	Water(char *texturePath);
	~Water(void);

	void Draw();
	void Update();
};

