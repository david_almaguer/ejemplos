#ifndef _CAJA_H_
#define _CAJA_H_

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

class Caja
{
public:
	GLuint textura[1];
	float tamanio;
	
	Caja(char* rutaTextura, float _tamanio);
	void Draw();
	void Update();
};

#endif