#ifndef _OPENGL_MANAGER_H
#define _OPENGL_MANAGER_H

#include <windows.h>
#include <gl\GLU.h>
#include <gl\GL.h>
#include "Caja.h"

class OpenGLManager
{
public:
	Caja *miCaja;
public:
	OpenGLManager(HWND hWnd)
	{
		glEnable(GL_TEXTURE_2D);							// Enable Texture Mapping ( NEW )
		glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
		glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
		glClearDepth(1.0f);									// Depth Buffer Setup
		glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
		glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations

		//habilitamos la iluminacion
		//glEnable(GL_LIGHTING);

		Initialize();
	}

	~OpenGLManager()
	{
		delete miCaja;
	}

	void Initialize()
	{
		miCaja = new Caja("Texturas/caja.bmp", 2);
	}

	void Draw(HDC hDC)
	{
		//borramos el biffer de color y el z para el control de profundidad a la 
		//hora del render a nivel pixel.
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		GLfloat LightAmb[] = { 0.0, 0.0,0.0, 0.0 };
		glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmb);
		glLoadIdentity();
	
		miCaja->Draw();

		SwapBuffers(hDC);
	}
};
#endif 